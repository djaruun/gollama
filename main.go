package main

import (
	"embed"
	"log"

	pbfunctions "github.com/djaruun/gollama/pb_functions"

	"github.com/wailsapp/wails/v2"
	// "github.com/wailsapp/wails/v2/pkg/logger"
	// "github.com/wailsapp/wails/v2/pkg/router"
	"github.com/wailsapp/wails/v2/pkg/options"
	"github.com/wailsapp/wails/v2/pkg/options/assetserver"
	"github.com/wailsapp/wails/v2/pkg/options/windows"
	// "github.com/wailsapp/wails/v2/pkg/runtime"
)

// CorsMiddleware sets the necessary CORS headers
// func CorsMiddleware(logger *logger.Logger) router.Middleware {
// 	return func(next router.Handler) router.Handler {
// 		return func(message *router.Message) {
// 			message.SetHeader("Access-Control-Allow-Origin", "http://wails.localhost:34115")
// 			message.SetHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE")
// 			message.SetHeader("Access-Control-Allow-Headers", "Content-Type, Authorization")
//
// 			// Call the next middleware or handler
// 			next(message)
// 		}
// 	}
// }

//go:embed all:frontend/build
var assets embed.FS

func main() {
	// Serve PocketBase instance
	go func() {
		err := pbfunctions.Serve()
		if err != nil {
			log.Fatal(err)
		}
	}()

	// Create an instance of the app structure
	app := NewApp()

	// Create application with options
	err := wails.Run(&options.App{
		Title:  "GOllama",
		Width:  1024,
		Height: 680,
		AssetServer: &assetserver.Options{
			Assets: assets,
		},
		BackgroundColour: &options.RGBA{R: 255, G: 255, B: 255, A: 0},
		OnStartup:        app.startup,
		Bind: []interface{}{
			app,
		},
		Windows: &windows.Options{
			WebviewIsTransparent: true,
			WindowIsTranslucent:  true,
			BackdropType:         windows.Mica,
		},
	})

	if err != nil {
		println("Error:", err.Error())
	}

	// runtime.EventsEmit(app.ctx, "event-test")
}
