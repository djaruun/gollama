package pbfunctions

import (
	"context"
	"log"

	"github.com/djaruun/gollama/ollama"
	"github.com/pocketbase/dbx"
	"github.com/pocketbase/pocketbase"
	"github.com/pocketbase/pocketbase/cmd"
	"github.com/pocketbase/pocketbase/models"
)

type History struct {
	ID        string `db:"id" json:"id"`
	Content   string `db:"content" json:"content"`
	Author    string `db:"author" json:"author"`
	Chat      string `db:"chat" json:"chat"`
	CreatedAt string `db:"created" json:"created"`
	UpdatedAt string `db:"updated" json:"updated"`
}

type Chat struct {
	ID        string `db:"id" json:"id"`
	Name      string `db:"name" json:"name"`
	Model     string `db:"model" json:"model"`
	CreatedAt string `db:"created" json:"created"`
	UpdatedAt string `db:"updated" json:"updated"`
}

var pb = pocketbase.New()

func GetHistoryFromChat(chat string) []History {
	var newHistory []History
	err := pb.Dao().DB().
		Select("*").
		From("history").
		Where(dbx.NewExp("chat = {:chat_id}", dbx.Params{"chat_id": chat})).
		OrderBy("updated ASC").
		All(&newHistory)
	if err != nil {
		log.Fatal(err)
	}

	return newHistory
}

func GetAllChats() []Chat {
	var newChats []Chat
	err := pb.Dao().DB().
		Select("*").
		From("chats").
		OrderBy("updated DESC").
		All(&newChats)
	if err != nil {
		log.Fatal(err)
	}

	return newChats
}

func SendMessageInChat(message string, author string, chat string, model string, ctx context.Context) {
	collection, _ := pb.Dao().FindCollectionByNameOrId("history")
	record := models.NewRecord(collection)

	record.Load(map[string]any{
		"content": message,
		"author":  author,
		"chat":    chat,
	})
	err := pb.Dao().SaveRecord(record)
	if err != nil {
		log.Fatal(err)
	}

	// return ollama.SendMessageToAI(message, model, ctx)
}

func CreateNewChat(name string, model string) {
	collection, _ := pb.Dao().FindCollectionByNameOrId("chats")
	record := models.NewRecord(collection)

	record.Load(map[string]any{
		"name":  name,
		"model": model,
	})
	err := pb.Dao().SaveRecord(record)
	if err != nil {
		log.Fatal(err)
	}
}

func Serve() error {
	err := pb.Bootstrap()
	if err != nil {
		return err
	}
	serveCmd := cmd.NewServeCommand(pb, true)
	err = serveCmd.Execute()
	if err != nil {
		return err
	}
	return nil
}
