package main

import (
	pbfunctions "github.com/djaruun/gollama/pb_functions"

	"context"
)

// App struct
type App struct {
	ctx context.Context
}

// Pseudo functions to bind to javascript
func (a *App) GetAllChats() []pbfunctions.Chat {
	return pbfunctions.GetAllChats()
}

func (a *App) GetHistoryFromChat(chat string) []pbfunctions.History {
	return pbfunctions.GetHistoryFromChat(chat)
}

func (a *App) SendMessageInChat(message string, author string, chat string, model string) {
	pbfunctions.SendMessageInChat(message, author, chat, model, a.ctx)
}

func (a *App) CreateNewChat(name string, model string) {
	pbfunctions.CreateNewChat(name, model)
}

// NewApp creates a new App application struct
func NewApp() *App {
	return &App{}
}

// startup is called when the app starts. The context is saved
// so we can call the runtime methods
func (a *App) startup(ctx context.Context) {
	a.ctx = ctx
}
