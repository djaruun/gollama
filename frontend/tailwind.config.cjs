/** @type {import('tailwindcss').Config}*/
const config = {
    content: ["./src/**/*.{html,js,svelte,ts}"],

    theme: {
        hljs: {
            theme: "atom-one-dynamic",
        },
        extend: {},
    },

    safelist: [{
        pattern: /hljs+/,
    }],

    plugins: [
        require("tailwind-highlightjs"),
        require("@tailwindcss/typography"),
    ],
};

module.exports = config
