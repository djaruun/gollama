export namespace pbfunctions {
	
	export class Chat {
	    id: string;
	    name: string;
	    model: string;
	    created: string;
	    updated: string;
	
	    static createFrom(source: any = {}) {
	        return new Chat(source);
	    }
	
	    constructor(source: any = {}) {
	        if ('string' === typeof source) source = JSON.parse(source);
	        this.id = source["id"];
	        this.name = source["name"];
	        this.model = source["model"];
	        this.created = source["created"];
	        this.updated = source["updated"];
	    }
	}
	export class History {
	    id: string;
	    content: string;
	    author: string;
	    chat: string;
	    created: string;
	    updated: string;
	
	    static createFrom(source: any = {}) {
	        return new History(source);
	    }
	
	    constructor(source: any = {}) {
	        if ('string' === typeof source) source = JSON.parse(source);
	        this.id = source["id"];
	        this.content = source["content"];
	        this.author = source["author"];
	        this.chat = source["chat"];
	        this.created = source["created"];
	        this.updated = source["updated"];
	    }
	}

}

