/**
 * Generates a relative date string based on the input date compared to the current date.
 *
 * @param {Date | number} date - The date to calculate the relative time from.
 * @return {string} The formatted relative date string.
 */
export function getRelativeDate(date: Date | number): string {
    const timeMs = typeof date === "number" ? date : date.getTime()
    const deltaSeconds = Math.round((timeMs - Date.now()) / 1000)
    
    const cutoffs = [60, 3600, 86400, 86400 * 7, 86400 * 30, 86400 * 365, Infinity]
    const units: Intl.RelativeTimeFormatUnit[] = ["second", "minute", "hour", "day", "week", "month", "year"]
    const unitIndex = cutoffs.findIndex(cutoff => cutoff > Math.abs(deltaSeconds))
    const divisor = unitIndex ? cutoffs[unitIndex - 1] : 1
    
    const rtf = new Intl.RelativeTimeFormat(navigator.language, { numeric: "auto" })
    return rtf.format(Math.floor(deltaSeconds / divisor), units[unitIndex])
}

/**
 * Scrolls the given element to the bottom with optional delay.
 *
 * @param {HTMLDivElement | HTMLElement | Element} element - The element to scroll
 * @param {ScrollBehavior} behavior - The scroll behavior
 * @param {number} [delay=0] - The optional delay in milliseconds
 */
export function scrollToBottom(element: HTMLDivElement | HTMLElement | Element, behavior: ScrollBehavior, delay: number = 0) {
    setTimeout(() => {
        element.scrollTo({
          top: 6969420420,
          behavior: behavior,
        })
    }, delay);
}
